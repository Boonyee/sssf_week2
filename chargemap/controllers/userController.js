"use strict";
const userModel = require("../models/userModel");

const users = userModel.users;

const user_list_get = (req, res) => {
  const result = users.map((model) => {
    return { ...model, ...{ password: undefined } };
  });
  res.json(result);
};

const user_get = (req, res) => {
  const result = users.filter((model) => model.id == req.params.id);
  result[0] = {
    ...result[0],
    ...{ password: undefined },
  };
  res.json(result);
};

module.exports = {
  user_list_get,
  user_get,
};
