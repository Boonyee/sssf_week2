const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const stationSchema = new Schema({
  Location: {
    coordinates: { type: [Number], required: true },
    type: { type: String, enum: ["Point"], required: true },
  },
  Connections: [{ type: Schema.Types.ObjectId, ref: "Connection" }],
  Title: String,
  AddressLine1: String,
  Town: String,
  StateOrProvince: String,
  Postcode: String,
});

module.exports = mongoose.model("Station", stationSchema);
