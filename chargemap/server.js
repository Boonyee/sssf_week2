"use strict";
require("dotenv").config();
const express = require("express");
const app = express();
const port = 3000;
const stationRoute = require("./routes/stationRoute.js");
const authRoute = require("./routes/authRoute.js");
const db = require("./utils/db.js");
const passport = require("./utils/pass");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/station", stationRoute);
app.use("/auth", authRoute);

db.on("connected", () => {
  app.listen(port, () => console.log(`App listening on port ${port}!`));
});
