"use strict";
require("dotenv").config();
const express = require("express");
const app = express();
const port = 3000;
const catRoute = require("./routes/catRoute.js");
const userRoute = require("./routes/userRoute");
const cors = require("cors");
const db = require("./utils/db.js");

app.use(express.static("public_html"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/cats", catRoute);
app.use("/user", userRoute);

db.on("connected", () => {
  app.listen(port, () => console.log(`Example app listening on port ${port}!`));
});
