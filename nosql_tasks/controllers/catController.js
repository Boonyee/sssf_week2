"use strict";
// catController
const Cat = require("../models/Cat.js");

// const cats = catModel.cats;

const cat_list_get = async (req, res) => {
  try {
    const cat = {
      ...(req.query.name && { name: req.query.name }),
      ...(req.query.age && { age: { $gt: Number(req.query.age) } }),
      ...(req.query.gender && { gender: req.query.gender }),
      ...(req.query.color && { color: req.query.color }),
      ...(req.query.weight && { weight: { $gt: Number(req.query.weight) } }),
    };
    console.log(cat);
    const cats = await Cat.find(cat).exec();
    res.json(cats);
  } catch (err) {
    throw err;
  }
};

const cat_post = async (req, res) => {
  try {
    const cat = await Cat.create({
      name: req.body.name,
      age: req.body.age,
      gender: req.body.gender,
      color: req.body.color,
      weight: req.body.weight,
    });
    res.send(`cat name ${cat.name} created with id: ${cat._id}`);
  } catch (err) {
    throw err;
  }
};

module.exports = {
  cat_list_get,
  cat_post,
};
