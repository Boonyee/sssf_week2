"use strict";
// catRoute

const router = require("express").Router();
const catController = require("../controllers/catController");

router.route("/").post(catController.cat_post).get(catController.cat_list_get);

module.exports = router;
